$random Garand/Ping
{
	Garand/Ping1
	Garand/Ping2
	Garand/Ping3
	Garand/Ping4
	Garand/Ping5
	Garand/Ping6
	Garand/Ping7
	Garand/Ping8
}

Garand/Ping1	"Sounds/M1PING1.ogg"
Garand/Ping2	"Sounds/M1PING2.ogg"
Garand/Ping3	"Sounds/M1PING3.ogg"
Garand/Ping4	"Sounds/M1PING4.ogg"
Garand/Ping5	"Sounds/M1PING5.ogg"
Garand/Ping6	"Sounds/M1PING6.ogg"
Garand/Ping7	"Sounds/M1PING7.ogg"
Garand/Ping8	"Sounds/M1PING8.ogg"

Garand/ClipOut	"Sounds/M1Clip1.ogg"
Garand/ClipSet	"Sounds/M1Clip2.ogg"
Garand/ClipIn	"Sounds/M1Clip3.ogg"

Garand/HandleBack	"Sounds/M1handle1.ogg"
Garand/HandleBack2	"Sounds/M1handle2.ogg"
Garand/HandleForward	"Sounds/M1handle3.ogg"

Garand/Fire	"Sounds/M1fire.wav"
